#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Test_OLED.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Test_OLED.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=i2c/i2c1ack.c i2c/i2c1clos.c i2c/i2c1dtrd.c i2c/i2c1eeap.c i2c/i2c1eebw.c i2c/i2c1eecr.c i2c/i2c1eepw.c i2c/i2c1eerr.c i2c/i2c1eesr.c i2c/i2c1gets.c i2c/i2c1idle.c i2c/i2c1nack.c i2c/i2c1open.c i2c/i2c1puts.c i2c/i2c1read.c i2c/i2c1rstr.c i2c/i2c1stop.c i2c/i2c1strt.c i2c/i2c1writ.c i2c/i2c2ack.c i2c/i2c2clos.c i2c/i2c2dtrd.c i2c/i2c2eeap.c i2c/i2c2eebw.c i2c/i2c2eecr.c i2c/i2c2eepw.c i2c/i2c2eerr.c i2c/i2c2eesr.c i2c/i2c2gets.c i2c/i2c2idle.c i2c/i2c2nack.c i2c/i2c2open.c i2c/i2c2puts.c i2c/i2c2read.c i2c/i2c2rstr.c i2c/i2c2stop.c i2c/i2c2strt.c i2c/i2c2writ.c i2c/i2c_ack.c i2c/i2c_clos.c i2c/i2c_dtrd.c i2c/i2c_eeap.c i2c/i2c_eebw.c i2c/i2c_eecr.c i2c/i2c_eepw.c i2c/i2c_eerr.c i2c/i2c_eesr.c i2c/i2c_gets.c i2c/i2c_idle.c i2c/i2c_nack.c i2c/i2c_open.c i2c/i2c_puts.c i2c/i2c_read.c i2c/i2c_rstr.c i2c/i2c_stop.c i2c/i2c_strt.c i2c/i2c_writ.c main_OLED.c num2str.c ssd1306xled.c ssd1306xled8x16.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/i2c/i2c1ack.p1 ${OBJECTDIR}/i2c/i2c1clos.p1 ${OBJECTDIR}/i2c/i2c1dtrd.p1 ${OBJECTDIR}/i2c/i2c1eeap.p1 ${OBJECTDIR}/i2c/i2c1eebw.p1 ${OBJECTDIR}/i2c/i2c1eecr.p1 ${OBJECTDIR}/i2c/i2c1eepw.p1 ${OBJECTDIR}/i2c/i2c1eerr.p1 ${OBJECTDIR}/i2c/i2c1eesr.p1 ${OBJECTDIR}/i2c/i2c1gets.p1 ${OBJECTDIR}/i2c/i2c1idle.p1 ${OBJECTDIR}/i2c/i2c1nack.p1 ${OBJECTDIR}/i2c/i2c1open.p1 ${OBJECTDIR}/i2c/i2c1puts.p1 ${OBJECTDIR}/i2c/i2c1read.p1 ${OBJECTDIR}/i2c/i2c1rstr.p1 ${OBJECTDIR}/i2c/i2c1stop.p1 ${OBJECTDIR}/i2c/i2c1strt.p1 ${OBJECTDIR}/i2c/i2c1writ.p1 ${OBJECTDIR}/i2c/i2c2ack.p1 ${OBJECTDIR}/i2c/i2c2clos.p1 ${OBJECTDIR}/i2c/i2c2dtrd.p1 ${OBJECTDIR}/i2c/i2c2eeap.p1 ${OBJECTDIR}/i2c/i2c2eebw.p1 ${OBJECTDIR}/i2c/i2c2eecr.p1 ${OBJECTDIR}/i2c/i2c2eepw.p1 ${OBJECTDIR}/i2c/i2c2eerr.p1 ${OBJECTDIR}/i2c/i2c2eesr.p1 ${OBJECTDIR}/i2c/i2c2gets.p1 ${OBJECTDIR}/i2c/i2c2idle.p1 ${OBJECTDIR}/i2c/i2c2nack.p1 ${OBJECTDIR}/i2c/i2c2open.p1 ${OBJECTDIR}/i2c/i2c2puts.p1 ${OBJECTDIR}/i2c/i2c2read.p1 ${OBJECTDIR}/i2c/i2c2rstr.p1 ${OBJECTDIR}/i2c/i2c2stop.p1 ${OBJECTDIR}/i2c/i2c2strt.p1 ${OBJECTDIR}/i2c/i2c2writ.p1 ${OBJECTDIR}/i2c/i2c_ack.p1 ${OBJECTDIR}/i2c/i2c_clos.p1 ${OBJECTDIR}/i2c/i2c_dtrd.p1 ${OBJECTDIR}/i2c/i2c_eeap.p1 ${OBJECTDIR}/i2c/i2c_eebw.p1 ${OBJECTDIR}/i2c/i2c_eecr.p1 ${OBJECTDIR}/i2c/i2c_eepw.p1 ${OBJECTDIR}/i2c/i2c_eerr.p1 ${OBJECTDIR}/i2c/i2c_eesr.p1 ${OBJECTDIR}/i2c/i2c_gets.p1 ${OBJECTDIR}/i2c/i2c_idle.p1 ${OBJECTDIR}/i2c/i2c_nack.p1 ${OBJECTDIR}/i2c/i2c_open.p1 ${OBJECTDIR}/i2c/i2c_puts.p1 ${OBJECTDIR}/i2c/i2c_read.p1 ${OBJECTDIR}/i2c/i2c_rstr.p1 ${OBJECTDIR}/i2c/i2c_stop.p1 ${OBJECTDIR}/i2c/i2c_strt.p1 ${OBJECTDIR}/i2c/i2c_writ.p1 ${OBJECTDIR}/main_OLED.p1 ${OBJECTDIR}/num2str.p1 ${OBJECTDIR}/ssd1306xled.p1 ${OBJECTDIR}/ssd1306xled8x16.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/i2c/i2c1ack.p1.d ${OBJECTDIR}/i2c/i2c1clos.p1.d ${OBJECTDIR}/i2c/i2c1dtrd.p1.d ${OBJECTDIR}/i2c/i2c1eeap.p1.d ${OBJECTDIR}/i2c/i2c1eebw.p1.d ${OBJECTDIR}/i2c/i2c1eecr.p1.d ${OBJECTDIR}/i2c/i2c1eepw.p1.d ${OBJECTDIR}/i2c/i2c1eerr.p1.d ${OBJECTDIR}/i2c/i2c1eesr.p1.d ${OBJECTDIR}/i2c/i2c1gets.p1.d ${OBJECTDIR}/i2c/i2c1idle.p1.d ${OBJECTDIR}/i2c/i2c1nack.p1.d ${OBJECTDIR}/i2c/i2c1open.p1.d ${OBJECTDIR}/i2c/i2c1puts.p1.d ${OBJECTDIR}/i2c/i2c1read.p1.d ${OBJECTDIR}/i2c/i2c1rstr.p1.d ${OBJECTDIR}/i2c/i2c1stop.p1.d ${OBJECTDIR}/i2c/i2c1strt.p1.d ${OBJECTDIR}/i2c/i2c1writ.p1.d ${OBJECTDIR}/i2c/i2c2ack.p1.d ${OBJECTDIR}/i2c/i2c2clos.p1.d ${OBJECTDIR}/i2c/i2c2dtrd.p1.d ${OBJECTDIR}/i2c/i2c2eeap.p1.d ${OBJECTDIR}/i2c/i2c2eebw.p1.d ${OBJECTDIR}/i2c/i2c2eecr.p1.d ${OBJECTDIR}/i2c/i2c2eepw.p1.d ${OBJECTDIR}/i2c/i2c2eerr.p1.d ${OBJECTDIR}/i2c/i2c2eesr.p1.d ${OBJECTDIR}/i2c/i2c2gets.p1.d ${OBJECTDIR}/i2c/i2c2idle.p1.d ${OBJECTDIR}/i2c/i2c2nack.p1.d ${OBJECTDIR}/i2c/i2c2open.p1.d ${OBJECTDIR}/i2c/i2c2puts.p1.d ${OBJECTDIR}/i2c/i2c2read.p1.d ${OBJECTDIR}/i2c/i2c2rstr.p1.d ${OBJECTDIR}/i2c/i2c2stop.p1.d ${OBJECTDIR}/i2c/i2c2strt.p1.d ${OBJECTDIR}/i2c/i2c2writ.p1.d ${OBJECTDIR}/i2c/i2c_ack.p1.d ${OBJECTDIR}/i2c/i2c_clos.p1.d ${OBJECTDIR}/i2c/i2c_dtrd.p1.d ${OBJECTDIR}/i2c/i2c_eeap.p1.d ${OBJECTDIR}/i2c/i2c_eebw.p1.d ${OBJECTDIR}/i2c/i2c_eecr.p1.d ${OBJECTDIR}/i2c/i2c_eepw.p1.d ${OBJECTDIR}/i2c/i2c_eerr.p1.d ${OBJECTDIR}/i2c/i2c_eesr.p1.d ${OBJECTDIR}/i2c/i2c_gets.p1.d ${OBJECTDIR}/i2c/i2c_idle.p1.d ${OBJECTDIR}/i2c/i2c_nack.p1.d ${OBJECTDIR}/i2c/i2c_open.p1.d ${OBJECTDIR}/i2c/i2c_puts.p1.d ${OBJECTDIR}/i2c/i2c_read.p1.d ${OBJECTDIR}/i2c/i2c_rstr.p1.d ${OBJECTDIR}/i2c/i2c_stop.p1.d ${OBJECTDIR}/i2c/i2c_strt.p1.d ${OBJECTDIR}/i2c/i2c_writ.p1.d ${OBJECTDIR}/main_OLED.p1.d ${OBJECTDIR}/num2str.p1.d ${OBJECTDIR}/ssd1306xled.p1.d ${OBJECTDIR}/ssd1306xled8x16.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/i2c/i2c1ack.p1 ${OBJECTDIR}/i2c/i2c1clos.p1 ${OBJECTDIR}/i2c/i2c1dtrd.p1 ${OBJECTDIR}/i2c/i2c1eeap.p1 ${OBJECTDIR}/i2c/i2c1eebw.p1 ${OBJECTDIR}/i2c/i2c1eecr.p1 ${OBJECTDIR}/i2c/i2c1eepw.p1 ${OBJECTDIR}/i2c/i2c1eerr.p1 ${OBJECTDIR}/i2c/i2c1eesr.p1 ${OBJECTDIR}/i2c/i2c1gets.p1 ${OBJECTDIR}/i2c/i2c1idle.p1 ${OBJECTDIR}/i2c/i2c1nack.p1 ${OBJECTDIR}/i2c/i2c1open.p1 ${OBJECTDIR}/i2c/i2c1puts.p1 ${OBJECTDIR}/i2c/i2c1read.p1 ${OBJECTDIR}/i2c/i2c1rstr.p1 ${OBJECTDIR}/i2c/i2c1stop.p1 ${OBJECTDIR}/i2c/i2c1strt.p1 ${OBJECTDIR}/i2c/i2c1writ.p1 ${OBJECTDIR}/i2c/i2c2ack.p1 ${OBJECTDIR}/i2c/i2c2clos.p1 ${OBJECTDIR}/i2c/i2c2dtrd.p1 ${OBJECTDIR}/i2c/i2c2eeap.p1 ${OBJECTDIR}/i2c/i2c2eebw.p1 ${OBJECTDIR}/i2c/i2c2eecr.p1 ${OBJECTDIR}/i2c/i2c2eepw.p1 ${OBJECTDIR}/i2c/i2c2eerr.p1 ${OBJECTDIR}/i2c/i2c2eesr.p1 ${OBJECTDIR}/i2c/i2c2gets.p1 ${OBJECTDIR}/i2c/i2c2idle.p1 ${OBJECTDIR}/i2c/i2c2nack.p1 ${OBJECTDIR}/i2c/i2c2open.p1 ${OBJECTDIR}/i2c/i2c2puts.p1 ${OBJECTDIR}/i2c/i2c2read.p1 ${OBJECTDIR}/i2c/i2c2rstr.p1 ${OBJECTDIR}/i2c/i2c2stop.p1 ${OBJECTDIR}/i2c/i2c2strt.p1 ${OBJECTDIR}/i2c/i2c2writ.p1 ${OBJECTDIR}/i2c/i2c_ack.p1 ${OBJECTDIR}/i2c/i2c_clos.p1 ${OBJECTDIR}/i2c/i2c_dtrd.p1 ${OBJECTDIR}/i2c/i2c_eeap.p1 ${OBJECTDIR}/i2c/i2c_eebw.p1 ${OBJECTDIR}/i2c/i2c_eecr.p1 ${OBJECTDIR}/i2c/i2c_eepw.p1 ${OBJECTDIR}/i2c/i2c_eerr.p1 ${OBJECTDIR}/i2c/i2c_eesr.p1 ${OBJECTDIR}/i2c/i2c_gets.p1 ${OBJECTDIR}/i2c/i2c_idle.p1 ${OBJECTDIR}/i2c/i2c_nack.p1 ${OBJECTDIR}/i2c/i2c_open.p1 ${OBJECTDIR}/i2c/i2c_puts.p1 ${OBJECTDIR}/i2c/i2c_read.p1 ${OBJECTDIR}/i2c/i2c_rstr.p1 ${OBJECTDIR}/i2c/i2c_stop.p1 ${OBJECTDIR}/i2c/i2c_strt.p1 ${OBJECTDIR}/i2c/i2c_writ.p1 ${OBJECTDIR}/main_OLED.p1 ${OBJECTDIR}/num2str.p1 ${OBJECTDIR}/ssd1306xled.p1 ${OBJECTDIR}/ssd1306xled8x16.p1

# Source Files
SOURCEFILES=i2c/i2c1ack.c i2c/i2c1clos.c i2c/i2c1dtrd.c i2c/i2c1eeap.c i2c/i2c1eebw.c i2c/i2c1eecr.c i2c/i2c1eepw.c i2c/i2c1eerr.c i2c/i2c1eesr.c i2c/i2c1gets.c i2c/i2c1idle.c i2c/i2c1nack.c i2c/i2c1open.c i2c/i2c1puts.c i2c/i2c1read.c i2c/i2c1rstr.c i2c/i2c1stop.c i2c/i2c1strt.c i2c/i2c1writ.c i2c/i2c2ack.c i2c/i2c2clos.c i2c/i2c2dtrd.c i2c/i2c2eeap.c i2c/i2c2eebw.c i2c/i2c2eecr.c i2c/i2c2eepw.c i2c/i2c2eerr.c i2c/i2c2eesr.c i2c/i2c2gets.c i2c/i2c2idle.c i2c/i2c2nack.c i2c/i2c2open.c i2c/i2c2puts.c i2c/i2c2read.c i2c/i2c2rstr.c i2c/i2c2stop.c i2c/i2c2strt.c i2c/i2c2writ.c i2c/i2c_ack.c i2c/i2c_clos.c i2c/i2c_dtrd.c i2c/i2c_eeap.c i2c/i2c_eebw.c i2c/i2c_eecr.c i2c/i2c_eepw.c i2c/i2c_eerr.c i2c/i2c_eesr.c i2c/i2c_gets.c i2c/i2c_idle.c i2c/i2c_nack.c i2c/i2c_open.c i2c/i2c_puts.c i2c/i2c_read.c i2c/i2c_rstr.c i2c/i2c_stop.c i2c/i2c_strt.c i2c/i2c_writ.c main_OLED.c num2str.c ssd1306xled.c ssd1306xled8x16.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/Test_OLED.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F4550
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/i2c/i2c1ack.p1: i2c/i2c1ack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1ack.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1ack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1ack.p1  i2c/i2c1ack.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1ack.d ${OBJECTDIR}/i2c/i2c1ack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1ack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1clos.p1: i2c/i2c1clos.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1clos.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1clos.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1clos.p1  i2c/i2c1clos.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1clos.d ${OBJECTDIR}/i2c/i2c1clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1dtrd.p1: i2c/i2c1dtrd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1dtrd.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1dtrd.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1dtrd.p1  i2c/i2c1dtrd.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1dtrd.d ${OBJECTDIR}/i2c/i2c1dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1eeap.p1: i2c/i2c1eeap.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1eeap.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1eeap.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1eeap.p1  i2c/i2c1eeap.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1eeap.d ${OBJECTDIR}/i2c/i2c1eeap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1eeap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1eebw.p1: i2c/i2c1eebw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1eebw.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1eebw.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1eebw.p1  i2c/i2c1eebw.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1eebw.d ${OBJECTDIR}/i2c/i2c1eebw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1eebw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1eecr.p1: i2c/i2c1eecr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1eecr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1eecr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1eecr.p1  i2c/i2c1eecr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1eecr.d ${OBJECTDIR}/i2c/i2c1eecr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1eecr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1eepw.p1: i2c/i2c1eepw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1eepw.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1eepw.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1eepw.p1  i2c/i2c1eepw.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1eepw.d ${OBJECTDIR}/i2c/i2c1eepw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1eepw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1eerr.p1: i2c/i2c1eerr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1eerr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1eerr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1eerr.p1  i2c/i2c1eerr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1eerr.d ${OBJECTDIR}/i2c/i2c1eerr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1eerr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1eesr.p1: i2c/i2c1eesr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1eesr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1eesr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1eesr.p1  i2c/i2c1eesr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1eesr.d ${OBJECTDIR}/i2c/i2c1eesr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1eesr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1gets.p1: i2c/i2c1gets.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1gets.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1gets.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1gets.p1  i2c/i2c1gets.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1gets.d ${OBJECTDIR}/i2c/i2c1gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1idle.p1: i2c/i2c1idle.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1idle.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1idle.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1idle.p1  i2c/i2c1idle.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1idle.d ${OBJECTDIR}/i2c/i2c1idle.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1idle.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1nack.p1: i2c/i2c1nack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1nack.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1nack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1nack.p1  i2c/i2c1nack.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1nack.d ${OBJECTDIR}/i2c/i2c1nack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1nack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1open.p1: i2c/i2c1open.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1open.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1open.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1open.p1  i2c/i2c1open.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1open.d ${OBJECTDIR}/i2c/i2c1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1puts.p1: i2c/i2c1puts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1puts.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1puts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1puts.p1  i2c/i2c1puts.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1puts.d ${OBJECTDIR}/i2c/i2c1puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1read.p1: i2c/i2c1read.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1read.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1read.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1read.p1  i2c/i2c1read.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1read.d ${OBJECTDIR}/i2c/i2c1read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1rstr.p1: i2c/i2c1rstr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1rstr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1rstr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1rstr.p1  i2c/i2c1rstr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1rstr.d ${OBJECTDIR}/i2c/i2c1rstr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1rstr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1stop.p1: i2c/i2c1stop.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1stop.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1stop.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1stop.p1  i2c/i2c1stop.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1stop.d ${OBJECTDIR}/i2c/i2c1stop.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1stop.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1strt.p1: i2c/i2c1strt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1strt.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1strt.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1strt.p1  i2c/i2c1strt.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1strt.d ${OBJECTDIR}/i2c/i2c1strt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1strt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1writ.p1: i2c/i2c1writ.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1writ.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1writ.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1writ.p1  i2c/i2c1writ.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1writ.d ${OBJECTDIR}/i2c/i2c1writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2ack.p1: i2c/i2c2ack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2ack.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2ack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2ack.p1  i2c/i2c2ack.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2ack.d ${OBJECTDIR}/i2c/i2c2ack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2ack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2clos.p1: i2c/i2c2clos.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2clos.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2clos.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2clos.p1  i2c/i2c2clos.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2clos.d ${OBJECTDIR}/i2c/i2c2clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2dtrd.p1: i2c/i2c2dtrd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2dtrd.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2dtrd.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2dtrd.p1  i2c/i2c2dtrd.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2dtrd.d ${OBJECTDIR}/i2c/i2c2dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2eeap.p1: i2c/i2c2eeap.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2eeap.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2eeap.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2eeap.p1  i2c/i2c2eeap.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2eeap.d ${OBJECTDIR}/i2c/i2c2eeap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2eeap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2eebw.p1: i2c/i2c2eebw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2eebw.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2eebw.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2eebw.p1  i2c/i2c2eebw.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2eebw.d ${OBJECTDIR}/i2c/i2c2eebw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2eebw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2eecr.p1: i2c/i2c2eecr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2eecr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2eecr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2eecr.p1  i2c/i2c2eecr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2eecr.d ${OBJECTDIR}/i2c/i2c2eecr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2eecr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2eepw.p1: i2c/i2c2eepw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2eepw.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2eepw.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2eepw.p1  i2c/i2c2eepw.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2eepw.d ${OBJECTDIR}/i2c/i2c2eepw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2eepw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2eerr.p1: i2c/i2c2eerr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2eerr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2eerr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2eerr.p1  i2c/i2c2eerr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2eerr.d ${OBJECTDIR}/i2c/i2c2eerr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2eerr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2eesr.p1: i2c/i2c2eesr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2eesr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2eesr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2eesr.p1  i2c/i2c2eesr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2eesr.d ${OBJECTDIR}/i2c/i2c2eesr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2eesr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2gets.p1: i2c/i2c2gets.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2gets.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2gets.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2gets.p1  i2c/i2c2gets.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2gets.d ${OBJECTDIR}/i2c/i2c2gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2idle.p1: i2c/i2c2idle.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2idle.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2idle.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2idle.p1  i2c/i2c2idle.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2idle.d ${OBJECTDIR}/i2c/i2c2idle.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2idle.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2nack.p1: i2c/i2c2nack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2nack.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2nack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2nack.p1  i2c/i2c2nack.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2nack.d ${OBJECTDIR}/i2c/i2c2nack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2nack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2open.p1: i2c/i2c2open.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2open.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2open.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2open.p1  i2c/i2c2open.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2open.d ${OBJECTDIR}/i2c/i2c2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2puts.p1: i2c/i2c2puts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2puts.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2puts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2puts.p1  i2c/i2c2puts.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2puts.d ${OBJECTDIR}/i2c/i2c2puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2read.p1: i2c/i2c2read.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2read.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2read.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2read.p1  i2c/i2c2read.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2read.d ${OBJECTDIR}/i2c/i2c2read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2rstr.p1: i2c/i2c2rstr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2rstr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2rstr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2rstr.p1  i2c/i2c2rstr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2rstr.d ${OBJECTDIR}/i2c/i2c2rstr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2rstr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2stop.p1: i2c/i2c2stop.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2stop.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2stop.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2stop.p1  i2c/i2c2stop.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2stop.d ${OBJECTDIR}/i2c/i2c2stop.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2stop.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2strt.p1: i2c/i2c2strt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2strt.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2strt.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2strt.p1  i2c/i2c2strt.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2strt.d ${OBJECTDIR}/i2c/i2c2strt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2strt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2writ.p1: i2c/i2c2writ.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2writ.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2writ.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2writ.p1  i2c/i2c2writ.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2writ.d ${OBJECTDIR}/i2c/i2c2writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_ack.p1: i2c/i2c_ack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_ack.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_ack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_ack.p1  i2c/i2c_ack.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_ack.d ${OBJECTDIR}/i2c/i2c_ack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_ack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_clos.p1: i2c/i2c_clos.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_clos.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_clos.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_clos.p1  i2c/i2c_clos.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_clos.d ${OBJECTDIR}/i2c/i2c_clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_dtrd.p1: i2c/i2c_dtrd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_dtrd.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_dtrd.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_dtrd.p1  i2c/i2c_dtrd.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_dtrd.d ${OBJECTDIR}/i2c/i2c_dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_eeap.p1: i2c/i2c_eeap.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_eeap.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_eeap.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_eeap.p1  i2c/i2c_eeap.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_eeap.d ${OBJECTDIR}/i2c/i2c_eeap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_eeap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_eebw.p1: i2c/i2c_eebw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_eebw.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_eebw.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_eebw.p1  i2c/i2c_eebw.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_eebw.d ${OBJECTDIR}/i2c/i2c_eebw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_eebw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_eecr.p1: i2c/i2c_eecr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_eecr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_eecr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_eecr.p1  i2c/i2c_eecr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_eecr.d ${OBJECTDIR}/i2c/i2c_eecr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_eecr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_eepw.p1: i2c/i2c_eepw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_eepw.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_eepw.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_eepw.p1  i2c/i2c_eepw.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_eepw.d ${OBJECTDIR}/i2c/i2c_eepw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_eepw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_eerr.p1: i2c/i2c_eerr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_eerr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_eerr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_eerr.p1  i2c/i2c_eerr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_eerr.d ${OBJECTDIR}/i2c/i2c_eerr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_eerr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_eesr.p1: i2c/i2c_eesr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_eesr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_eesr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_eesr.p1  i2c/i2c_eesr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_eesr.d ${OBJECTDIR}/i2c/i2c_eesr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_eesr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_gets.p1: i2c/i2c_gets.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_gets.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_gets.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_gets.p1  i2c/i2c_gets.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_gets.d ${OBJECTDIR}/i2c/i2c_gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_idle.p1: i2c/i2c_idle.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_idle.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_idle.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_idle.p1  i2c/i2c_idle.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_idle.d ${OBJECTDIR}/i2c/i2c_idle.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_idle.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_nack.p1: i2c/i2c_nack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_nack.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_nack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_nack.p1  i2c/i2c_nack.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_nack.d ${OBJECTDIR}/i2c/i2c_nack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_nack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_open.p1: i2c/i2c_open.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_open.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_open.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_open.p1  i2c/i2c_open.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_open.d ${OBJECTDIR}/i2c/i2c_open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_puts.p1: i2c/i2c_puts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_puts.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_puts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_puts.p1  i2c/i2c_puts.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_puts.d ${OBJECTDIR}/i2c/i2c_puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_read.p1: i2c/i2c_read.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_read.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_read.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_read.p1  i2c/i2c_read.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_read.d ${OBJECTDIR}/i2c/i2c_read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_rstr.p1: i2c/i2c_rstr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_rstr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_rstr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_rstr.p1  i2c/i2c_rstr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_rstr.d ${OBJECTDIR}/i2c/i2c_rstr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_rstr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_stop.p1: i2c/i2c_stop.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_stop.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_stop.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_stop.p1  i2c/i2c_stop.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_stop.d ${OBJECTDIR}/i2c/i2c_stop.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_stop.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_strt.p1: i2c/i2c_strt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_strt.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_strt.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_strt.p1  i2c/i2c_strt.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_strt.d ${OBJECTDIR}/i2c/i2c_strt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_strt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_writ.p1: i2c/i2c_writ.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_writ.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_writ.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_writ.p1  i2c/i2c_writ.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_writ.d ${OBJECTDIR}/i2c/i2c_writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main_OLED.p1: main_OLED.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main_OLED.p1.d 
	@${RM} ${OBJECTDIR}/main_OLED.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/main_OLED.p1  main_OLED.c 
	@-${MV} ${OBJECTDIR}/main_OLED.d ${OBJECTDIR}/main_OLED.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main_OLED.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/num2str.p1: num2str.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/num2str.p1.d 
	@${RM} ${OBJECTDIR}/num2str.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/num2str.p1  num2str.c 
	@-${MV} ${OBJECTDIR}/num2str.d ${OBJECTDIR}/num2str.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/num2str.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ssd1306xled.p1: ssd1306xled.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/ssd1306xled.p1.d 
	@${RM} ${OBJECTDIR}/ssd1306xled.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/ssd1306xled.p1  ssd1306xled.c 
	@-${MV} ${OBJECTDIR}/ssd1306xled.d ${OBJECTDIR}/ssd1306xled.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ssd1306xled.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ssd1306xled8x16.p1: ssd1306xled8x16.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/ssd1306xled8x16.p1.d 
	@${RM} ${OBJECTDIR}/ssd1306xled8x16.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/ssd1306xled8x16.p1  ssd1306xled8x16.c 
	@-${MV} ${OBJECTDIR}/ssd1306xled8x16.d ${OBJECTDIR}/ssd1306xled8x16.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ssd1306xled8x16.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/i2c/i2c1ack.p1: i2c/i2c1ack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1ack.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1ack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1ack.p1  i2c/i2c1ack.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1ack.d ${OBJECTDIR}/i2c/i2c1ack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1ack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1clos.p1: i2c/i2c1clos.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1clos.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1clos.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1clos.p1  i2c/i2c1clos.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1clos.d ${OBJECTDIR}/i2c/i2c1clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1dtrd.p1: i2c/i2c1dtrd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1dtrd.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1dtrd.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1dtrd.p1  i2c/i2c1dtrd.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1dtrd.d ${OBJECTDIR}/i2c/i2c1dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1eeap.p1: i2c/i2c1eeap.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1eeap.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1eeap.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1eeap.p1  i2c/i2c1eeap.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1eeap.d ${OBJECTDIR}/i2c/i2c1eeap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1eeap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1eebw.p1: i2c/i2c1eebw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1eebw.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1eebw.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1eebw.p1  i2c/i2c1eebw.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1eebw.d ${OBJECTDIR}/i2c/i2c1eebw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1eebw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1eecr.p1: i2c/i2c1eecr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1eecr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1eecr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1eecr.p1  i2c/i2c1eecr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1eecr.d ${OBJECTDIR}/i2c/i2c1eecr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1eecr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1eepw.p1: i2c/i2c1eepw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1eepw.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1eepw.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1eepw.p1  i2c/i2c1eepw.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1eepw.d ${OBJECTDIR}/i2c/i2c1eepw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1eepw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1eerr.p1: i2c/i2c1eerr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1eerr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1eerr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1eerr.p1  i2c/i2c1eerr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1eerr.d ${OBJECTDIR}/i2c/i2c1eerr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1eerr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1eesr.p1: i2c/i2c1eesr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1eesr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1eesr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1eesr.p1  i2c/i2c1eesr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1eesr.d ${OBJECTDIR}/i2c/i2c1eesr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1eesr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1gets.p1: i2c/i2c1gets.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1gets.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1gets.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1gets.p1  i2c/i2c1gets.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1gets.d ${OBJECTDIR}/i2c/i2c1gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1idle.p1: i2c/i2c1idle.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1idle.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1idle.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1idle.p1  i2c/i2c1idle.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1idle.d ${OBJECTDIR}/i2c/i2c1idle.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1idle.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1nack.p1: i2c/i2c1nack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1nack.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1nack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1nack.p1  i2c/i2c1nack.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1nack.d ${OBJECTDIR}/i2c/i2c1nack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1nack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1open.p1: i2c/i2c1open.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1open.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1open.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1open.p1  i2c/i2c1open.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1open.d ${OBJECTDIR}/i2c/i2c1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1puts.p1: i2c/i2c1puts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1puts.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1puts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1puts.p1  i2c/i2c1puts.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1puts.d ${OBJECTDIR}/i2c/i2c1puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1read.p1: i2c/i2c1read.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1read.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1read.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1read.p1  i2c/i2c1read.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1read.d ${OBJECTDIR}/i2c/i2c1read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1rstr.p1: i2c/i2c1rstr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1rstr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1rstr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1rstr.p1  i2c/i2c1rstr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1rstr.d ${OBJECTDIR}/i2c/i2c1rstr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1rstr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1stop.p1: i2c/i2c1stop.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1stop.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1stop.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1stop.p1  i2c/i2c1stop.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1stop.d ${OBJECTDIR}/i2c/i2c1stop.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1stop.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1strt.p1: i2c/i2c1strt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1strt.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1strt.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1strt.p1  i2c/i2c1strt.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1strt.d ${OBJECTDIR}/i2c/i2c1strt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1strt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c1writ.p1: i2c/i2c1writ.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c1writ.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c1writ.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c1writ.p1  i2c/i2c1writ.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c1writ.d ${OBJECTDIR}/i2c/i2c1writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c1writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2ack.p1: i2c/i2c2ack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2ack.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2ack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2ack.p1  i2c/i2c2ack.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2ack.d ${OBJECTDIR}/i2c/i2c2ack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2ack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2clos.p1: i2c/i2c2clos.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2clos.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2clos.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2clos.p1  i2c/i2c2clos.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2clos.d ${OBJECTDIR}/i2c/i2c2clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2dtrd.p1: i2c/i2c2dtrd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2dtrd.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2dtrd.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2dtrd.p1  i2c/i2c2dtrd.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2dtrd.d ${OBJECTDIR}/i2c/i2c2dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2eeap.p1: i2c/i2c2eeap.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2eeap.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2eeap.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2eeap.p1  i2c/i2c2eeap.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2eeap.d ${OBJECTDIR}/i2c/i2c2eeap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2eeap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2eebw.p1: i2c/i2c2eebw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2eebw.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2eebw.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2eebw.p1  i2c/i2c2eebw.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2eebw.d ${OBJECTDIR}/i2c/i2c2eebw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2eebw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2eecr.p1: i2c/i2c2eecr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2eecr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2eecr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2eecr.p1  i2c/i2c2eecr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2eecr.d ${OBJECTDIR}/i2c/i2c2eecr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2eecr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2eepw.p1: i2c/i2c2eepw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2eepw.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2eepw.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2eepw.p1  i2c/i2c2eepw.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2eepw.d ${OBJECTDIR}/i2c/i2c2eepw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2eepw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2eerr.p1: i2c/i2c2eerr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2eerr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2eerr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2eerr.p1  i2c/i2c2eerr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2eerr.d ${OBJECTDIR}/i2c/i2c2eerr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2eerr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2eesr.p1: i2c/i2c2eesr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2eesr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2eesr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2eesr.p1  i2c/i2c2eesr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2eesr.d ${OBJECTDIR}/i2c/i2c2eesr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2eesr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2gets.p1: i2c/i2c2gets.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2gets.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2gets.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2gets.p1  i2c/i2c2gets.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2gets.d ${OBJECTDIR}/i2c/i2c2gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2idle.p1: i2c/i2c2idle.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2idle.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2idle.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2idle.p1  i2c/i2c2idle.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2idle.d ${OBJECTDIR}/i2c/i2c2idle.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2idle.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2nack.p1: i2c/i2c2nack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2nack.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2nack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2nack.p1  i2c/i2c2nack.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2nack.d ${OBJECTDIR}/i2c/i2c2nack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2nack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2open.p1: i2c/i2c2open.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2open.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2open.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2open.p1  i2c/i2c2open.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2open.d ${OBJECTDIR}/i2c/i2c2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2puts.p1: i2c/i2c2puts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2puts.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2puts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2puts.p1  i2c/i2c2puts.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2puts.d ${OBJECTDIR}/i2c/i2c2puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2read.p1: i2c/i2c2read.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2read.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2read.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2read.p1  i2c/i2c2read.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2read.d ${OBJECTDIR}/i2c/i2c2read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2rstr.p1: i2c/i2c2rstr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2rstr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2rstr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2rstr.p1  i2c/i2c2rstr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2rstr.d ${OBJECTDIR}/i2c/i2c2rstr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2rstr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2stop.p1: i2c/i2c2stop.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2stop.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2stop.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2stop.p1  i2c/i2c2stop.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2stop.d ${OBJECTDIR}/i2c/i2c2stop.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2stop.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2strt.p1: i2c/i2c2strt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2strt.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2strt.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2strt.p1  i2c/i2c2strt.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2strt.d ${OBJECTDIR}/i2c/i2c2strt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2strt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c2writ.p1: i2c/i2c2writ.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c2writ.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c2writ.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c2writ.p1  i2c/i2c2writ.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c2writ.d ${OBJECTDIR}/i2c/i2c2writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c2writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_ack.p1: i2c/i2c_ack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_ack.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_ack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_ack.p1  i2c/i2c_ack.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_ack.d ${OBJECTDIR}/i2c/i2c_ack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_ack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_clos.p1: i2c/i2c_clos.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_clos.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_clos.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_clos.p1  i2c/i2c_clos.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_clos.d ${OBJECTDIR}/i2c/i2c_clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_dtrd.p1: i2c/i2c_dtrd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_dtrd.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_dtrd.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_dtrd.p1  i2c/i2c_dtrd.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_dtrd.d ${OBJECTDIR}/i2c/i2c_dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_eeap.p1: i2c/i2c_eeap.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_eeap.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_eeap.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_eeap.p1  i2c/i2c_eeap.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_eeap.d ${OBJECTDIR}/i2c/i2c_eeap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_eeap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_eebw.p1: i2c/i2c_eebw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_eebw.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_eebw.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_eebw.p1  i2c/i2c_eebw.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_eebw.d ${OBJECTDIR}/i2c/i2c_eebw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_eebw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_eecr.p1: i2c/i2c_eecr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_eecr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_eecr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_eecr.p1  i2c/i2c_eecr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_eecr.d ${OBJECTDIR}/i2c/i2c_eecr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_eecr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_eepw.p1: i2c/i2c_eepw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_eepw.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_eepw.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_eepw.p1  i2c/i2c_eepw.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_eepw.d ${OBJECTDIR}/i2c/i2c_eepw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_eepw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_eerr.p1: i2c/i2c_eerr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_eerr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_eerr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_eerr.p1  i2c/i2c_eerr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_eerr.d ${OBJECTDIR}/i2c/i2c_eerr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_eerr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_eesr.p1: i2c/i2c_eesr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_eesr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_eesr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_eesr.p1  i2c/i2c_eesr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_eesr.d ${OBJECTDIR}/i2c/i2c_eesr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_eesr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_gets.p1: i2c/i2c_gets.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_gets.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_gets.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_gets.p1  i2c/i2c_gets.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_gets.d ${OBJECTDIR}/i2c/i2c_gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_idle.p1: i2c/i2c_idle.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_idle.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_idle.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_idle.p1  i2c/i2c_idle.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_idle.d ${OBJECTDIR}/i2c/i2c_idle.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_idle.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_nack.p1: i2c/i2c_nack.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_nack.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_nack.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_nack.p1  i2c/i2c_nack.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_nack.d ${OBJECTDIR}/i2c/i2c_nack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_nack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_open.p1: i2c/i2c_open.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_open.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_open.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_open.p1  i2c/i2c_open.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_open.d ${OBJECTDIR}/i2c/i2c_open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_puts.p1: i2c/i2c_puts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_puts.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_puts.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_puts.p1  i2c/i2c_puts.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_puts.d ${OBJECTDIR}/i2c/i2c_puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_read.p1: i2c/i2c_read.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_read.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_read.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_read.p1  i2c/i2c_read.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_read.d ${OBJECTDIR}/i2c/i2c_read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_rstr.p1: i2c/i2c_rstr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_rstr.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_rstr.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_rstr.p1  i2c/i2c_rstr.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_rstr.d ${OBJECTDIR}/i2c/i2c_rstr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_rstr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_stop.p1: i2c/i2c_stop.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_stop.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_stop.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_stop.p1  i2c/i2c_stop.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_stop.d ${OBJECTDIR}/i2c/i2c_stop.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_stop.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_strt.p1: i2c/i2c_strt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_strt.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_strt.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_strt.p1  i2c/i2c_strt.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_strt.d ${OBJECTDIR}/i2c/i2c_strt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_strt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/i2c/i2c_writ.p1: i2c/i2c_writ.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/i2c" 
	@${RM} ${OBJECTDIR}/i2c/i2c_writ.p1.d 
	@${RM} ${OBJECTDIR}/i2c/i2c_writ.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/i2c/i2c_writ.p1  i2c/i2c_writ.c 
	@-${MV} ${OBJECTDIR}/i2c/i2c_writ.d ${OBJECTDIR}/i2c/i2c_writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/i2c/i2c_writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main_OLED.p1: main_OLED.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main_OLED.p1.d 
	@${RM} ${OBJECTDIR}/main_OLED.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/main_OLED.p1  main_OLED.c 
	@-${MV} ${OBJECTDIR}/main_OLED.d ${OBJECTDIR}/main_OLED.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main_OLED.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/num2str.p1: num2str.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/num2str.p1.d 
	@${RM} ${OBJECTDIR}/num2str.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/num2str.p1  num2str.c 
	@-${MV} ${OBJECTDIR}/num2str.d ${OBJECTDIR}/num2str.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/num2str.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ssd1306xled.p1: ssd1306xled.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/ssd1306xled.p1.d 
	@${RM} ${OBJECTDIR}/ssd1306xled.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/ssd1306xled.p1  ssd1306xled.c 
	@-${MV} ${OBJECTDIR}/ssd1306xled.d ${OBJECTDIR}/ssd1306xled.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ssd1306xled.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ssd1306xled8x16.p1: ssd1306xled8x16.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/ssd1306xled8x16.p1.d 
	@${RM} ${OBJECTDIR}/ssd1306xled8x16.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/ssd1306xled8x16.p1  ssd1306xled8x16.c 
	@-${MV} ${OBJECTDIR}/ssd1306xled8x16.d ${OBJECTDIR}/ssd1306xled8x16.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ssd1306xled8x16.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/Test_OLED.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/Test_OLED.X.${IMAGE_TYPE}.map  -D__DEBUG=1 --debugger=none  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"        $(COMPARISON_BUILD) --memorysummary dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -odist/${CND_CONF}/${IMAGE_TYPE}/Test_OLED.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/Test_OLED.X.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/Test_OLED.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/Test_OLED.X.${IMAGE_TYPE}.map  --double=24 --float=24 --emi=wordwrite --opt=default,+asm,+asmfile,-speed,+space,-debug --addrqual=ignore --mode=free -P -N255 -I"C:/Users/Brandy/MPLABXProjects/OLED_Test.X" --warn=0 --asmlist -DXPRJ_default=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --codeoffset=0x1000 --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,+plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     $(COMPARISON_BUILD) --memorysummary dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -odist/${CND_CONF}/${IMAGE_TYPE}/Test_OLED.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
