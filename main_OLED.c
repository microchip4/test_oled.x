/*
 * File:   main_OLED.c
 * Author: Brandy
 *
 * Created on 20 de enero de 2017, 14:04
 */

#define CLOCK_20MHZ
#include <xc.h>
#include "Alteri.h"
#include <stdio.h>
#include "ssd1306xled.h"
#include "ssd1306xled8x16.h"
#include "img0_128x64c1.h"
#include "img1_128x64c1.h"

void main(void) {
	// Small delay is necessary if ssd1306_init is the first operation in the application.
	delay_ms(40);
	ssd1306_init();

	// ---- Main Loop ----

	for (;;) {
		
		ssd1306_clear(); 
        delay_ms(200);

		// ---- Fill out screen with stripes ----
		char p = 0xff;
		for (char i = 0; i < 8; i++) {
			p = (p >> 1);
			ssd1306_fill(~p);
		}
		delay_ms(400);

		// ---- Fill out screen with patters ----
		ssd1306_fill(0xAA); delay_ms(400);
		ssd1306_fill2(0x55, 0xAA); 
        delay_ms(400);
		ssd1306_fill4(0xCC, 0xCC, 0x33, 0x33); 
        delay_ms(400);
		delay_ms(1000);

		// ---- Print some small numbers on the screen ----
		int n1 = 0;
		for (char j = 0; j < 8; j++) {
			ssd1306_setpos(0, j);
			for (char i = 0; i < 7; i++) {
				ssd1306_numdec(n1++);
				ssd1306_string(" ");
			}
		}
		delay_ms(1000);
		
		// ---- Print some large numbers on the screen ----
		ssd1306_fill2(0xAA, 0x55);	// Fill screen
		int n2 = 199;
		for (char j = 1; j < 7; j++) {
			ssd1306_setpos(10, j);
			for (char i = 0; i < 3; i++) {
				ssd1306_numdecp(n2);
				ssd1306_string(" ");
				n2 += 567;
			}
		}
		delay_ms(1000);
		
		// ---- Print some variables on the screen ----
		ssd1306_fill4(0xC0, 0x030, 0x0C, 0x03); 
        delay_ms(400);
		int n3 = 0;
		for (char i = 0; i < 163; i++) {
			ssd1306_setpos(44, 3);
			ssd1306_string("a=");
			ssd1306_numdecp(n3);
			ssd1306_setpos(44, 4);
			ssd1306_string("b=");
			ssd1306_numdecp(0xffff - n3);
			n3 += (n3 * 3) / 33 + 1;
		}
		delay_ms(1000);

		// ---- Print some small and large text on the screen ----
		ssd1306_clear(); 
        delay_ms(200);
		ssd1306_setpos(34, 0);	ssd1306_string_font6x8("This is the");
		ssd1306_string_font8x16xy(10, 1, "Tinusaur");
		ssd1306_setpos(80, 2);	ssd1306_string_font6x8("project");
		ssd1306_setpos(0, 4);	ssd1306_string_font6x8("The quick start       platform for your    next awesome project");
		ssd1306_setpos(8, 7);	ssd1306_string_font6x8("http://tinusaur.org");
        delay_ms(6000);
		
		// ---- Draw bitmap on the screen ----
		ssd1306_draw_bmp(0,0,128,8, img1_128x64c1);
		delay_ms(4000);

		// ---- Draw bitmap on the screen ----
		ssd1306_draw_bmp(0,0,128,8, img0_128x64c1);
		delay_ms(6000);
	}
	//return 0;
}
